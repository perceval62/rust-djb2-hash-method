use std::env;


fn hash(to_hash: String) -> u64
{
    //L'État initial est le 1000e nombre premier
    let mut hash: u64 = 3389;
    for x in 0..(to_hash.as_str().char_indices().count())
    {
        hash = ((hash * 32) + hash) + u64::from(to_hash.as_bytes()[x]);
    }
    return hash;
}

fn main()
{

    let argumentsCommandeDeLigne: Vec<String> = env::args().collect();
    println!("Rust implementation of the djb2 hashing algorithm");
    println!("*************************************************");
    println!("Vincent Perrier  19/05/2019");
    if argumentsCommandeDeLigne.len() > 1
    {
        let string_to_hash = argumentsCommandeDeLigne[1].clone();
        println!("Argument reçu: {}", string_to_hash);
        let hash: u64 = hash(string_to_hash);
        println!("Hash: {}", hash);
    }
    else
    {
        println!("Argument de ligne de commande manquant");
        return
    }
}
